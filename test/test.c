/*------------------------------------------------------------------------------
| INFORMATIONS
|
| filename: main.c
|
| author:   Krzysztof Tkaczenko
| created:  Feb 23, 2019
 `----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
| INCLUDES
`-----------------------------------------------------------------------------*/

#include <string.h>
#include "unity.h"
#include "flash.h"
#include "flash_driver.h"

/*------------------------------------------------------------------------------
| DEFINES
`-----------------------------------------------------------------------------*/

#define FLASH_PAGE_0     0x00
#define FLASH_PAGE_1     0x01
#define FLASH_PAGE_2     0x02
#define FLASH_PAGE_3     0x03
#define FLASH_PAGE_4     0x04

#define MEMORY_ADDR_0    0x0000
#define MEMORY_ADDR_1    0x0110
#define MEMORY_ADDR_2    0x0220
#define MEMORY_ADDR_3    0x0330
#define MEMORY_ADDR_W    0x0400

#define ATTR_ID_0        0x00
#define ATTR_ID_1        0x01
#define ATTR_ID_2        0x02
#define ATTR_ID_3        0x03
#define ATTR_ID_W        0x04

#define ATTR_ID_0_LEN    0x03
#define ATTR_ID_1_LEN    0x04
#define ATTR_ID_2_LEN    0x05
#define ATTR_ID_3_LEN    0x06
#define ATTR_W_LEN       0xFA

#define TEST_RUN_COUNT   0x80

#define LINE_SEPARATOR() printf("------------------" \
                                "------------------" \
                                "----------------\n");

/*------------------------------------------------------------------------------
| LOCAL VARIABLES
`-----------------------------------------------------------------------------*/

static uint8_t flash_page_data[FLASH_PAGE_SIZE];

static uint8_t flash_page_0_out[FLASH_PAGE_SIZE];
static uint8_t flash_page_1_out[FLASH_PAGE_SIZE];
static uint8_t flash_page_2_out[FLASH_PAGE_SIZE];
static uint8_t flash_page_3_out[FLASH_PAGE_SIZE];

static uint8_t attr_0_data[ATTR_ID_0_LEN] = {0x01, 0x02, 0x03};
static uint8_t attr_1_data[ATTR_ID_1_LEN] = {0x11, 0x22, 0x33, 0x44};
static uint8_t attr_2_data[ATTR_ID_2_LEN] = {0xAA, 0xBB, 0xCC, 0xDD, 0xEE};
static uint8_t attr_3_data[ATTR_ID_3_LEN] = {0xA1, 0xB2, 0xC3, 0xD4, 0xE5, 0xF6};

static uint8_t attr_0_out_len;
static uint8_t attr_1_out_len;
static uint8_t attr_2_out_len;
static uint8_t attr_3_out_len;

static uint8_t attr_0_out[ATTR_ID_0_LEN];
static uint8_t attr_1_out[ATTR_ID_1_LEN];
static uint8_t attr_2_out[ATTR_ID_2_LEN];
static uint8_t attr_3_out[ATTR_ID_3_LEN];

/*------------------------------------------------------------------------------
| TEST FUNCTIONS PROTOTYPES
`-----------------------------------------------------------------------------*/

static void modify_data(void);
static void test_initialization(void);
static void test_flash(void);
static void test_flash_driver(void);

/*------------------------------------------------------------------------------
| TEST FUNCTIONS
`-----------------------------------------------------------------------------*/

static void modify_data(void)
{
    attr_0_data[0]++;
    attr_0_data[1]++;
    attr_0_data[2]++;
    attr_1_data[0]++;
    attr_1_data[1]++;
    attr_1_data[2]++;
    attr_1_data[3]++;
    attr_2_data[0]++;
    attr_2_data[1]++;
    attr_2_data[2]++;
    attr_2_data[3]++;
    attr_2_data[4]++;
    attr_3_data[0]++;
    attr_3_data[1]++;
    attr_3_data[2]++;
    attr_3_data[3]++;
    attr_3_data[4]++;
    attr_3_data[5]++;
}

static void test_initialization(void)
{
    TEST_ASSERT_EQUAL_UINT8(INIT_SUCCESS, flash_init());
    TEST_ASSERT_EQUAL_UINT8(INIT_SUCCESS, flash_driver_init());

    UNITY_PRINT_EOL();
    LINE_SEPARATOR();
}

static void test_flash(void)
{
    memset(flash_page_data, 0xFF, sizeof(flash_page_data));
    memset(flash_page_0_out, 0x00, sizeof(flash_page_0_out));
    memset(flash_page_1_out, 0x00, sizeof(flash_page_1_out));
    memset(flash_page_2_out, 0x00, sizeof(flash_page_2_out));
    memset(flash_page_3_out, 0x00, sizeof(flash_page_3_out));
    memset(attr_0_out, 0x00, sizeof(attr_0_out));
    memset(attr_1_out, 0x00, sizeof(attr_1_out));
    memset(attr_2_out, 0x00, sizeof(attr_2_out));
    memset(attr_3_out, 0x00, sizeof(attr_3_out));

    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS,  flash_erase_page(FLASH_PAGE_0));
    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS,  flash_erase_page(FLASH_PAGE_1));
    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS,  flash_erase_page(FLASH_PAGE_2));
    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS,  flash_erase_page(FLASH_PAGE_3));
    TEST_ASSERT_EQUAL_UINT8(INVALID_PARAMS, flash_erase_page(FLASH_PAGE_4));

    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(flash_page_0_out, FLASH_PAGE_0 * FLASH_PAGE_SIZE, sizeof(flash_page_0_out)));
    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(flash_page_1_out, FLASH_PAGE_1 * FLASH_PAGE_SIZE, sizeof(flash_page_1_out)));
    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(flash_page_2_out, FLASH_PAGE_2 * FLASH_PAGE_SIZE, sizeof(flash_page_2_out)));
    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(flash_page_3_out, FLASH_PAGE_3 * FLASH_PAGE_SIZE, sizeof(flash_page_3_out)));

    TEST_ASSERT_EQUAL_UINT8_ARRAY(flash_page_data, flash_page_0_out, sizeof(flash_page_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(flash_page_data, flash_page_1_out, sizeof(flash_page_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(flash_page_data, flash_page_2_out, sizeof(flash_page_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(flash_page_data, flash_page_3_out, sizeof(flash_page_data));

    TEST_ASSERT_EQUAL_UINT8(WRITE_SUCCESS, flash_write_data(attr_0_data, MEMORY_ADDR_0, sizeof(attr_0_data)));
    TEST_ASSERT_EQUAL_UINT8(WRITE_SUCCESS, flash_write_data(attr_1_data, MEMORY_ADDR_1, sizeof(attr_1_data)));
    TEST_ASSERT_EQUAL_UINT8(WRITE_SUCCESS, flash_write_data(attr_2_data, MEMORY_ADDR_2, sizeof(attr_2_data)));
    TEST_ASSERT_EQUAL_UINT8(WRITE_SUCCESS, flash_write_data(attr_3_data, MEMORY_ADDR_3, sizeof(attr_3_data)));

    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(attr_0_out, MEMORY_ADDR_0, sizeof(attr_0_out)));
    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(attr_1_out, MEMORY_ADDR_1, sizeof(attr_1_out)));
    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(attr_2_out, MEMORY_ADDR_2, sizeof(attr_2_out)));
    TEST_ASSERT_EQUAL_UINT8(READ_SUCCESS, flash_read_data(attr_3_out, MEMORY_ADDR_3, sizeof(attr_3_out)));

    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_0_data, attr_0_out, sizeof(attr_0_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_1_data, attr_1_out, sizeof(attr_1_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_2_data, attr_2_out, sizeof(attr_2_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_3_data, attr_3_out, sizeof(attr_3_data));

    TEST_ASSERT_EQUAL_UINT8(INVALID_PARAMS, flash_read_data(attr_0_out, MEMORY_ADDR_W, sizeof(attr_0_out)));
    TEST_ASSERT_EQUAL_UINT8(INVALID_PARAMS, flash_write_data(attr_0_data, MEMORY_ADDR_W, sizeof(attr_0_data)));

    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS, flash_erase_page(FLASH_PAGE_0));
    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS, flash_erase_page(FLASH_PAGE_1));
    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS, flash_erase_page(FLASH_PAGE_2));
    TEST_ASSERT_EQUAL_UINT8(ERASE_SUCCESS, flash_erase_page(FLASH_PAGE_3));

    UNITY_PRINT_EOL();
    LINE_SEPARATOR();
}

static void test_flash_driver(void)
{
    memset(attr_0_out, 0x00, sizeof(attr_0_out));
    memset(attr_1_out, 0x00, sizeof(attr_1_out));
    memset(attr_2_out, 0x00, sizeof(attr_2_out));
    memset(attr_3_out, 0x00, sizeof(attr_3_out));

    TEST_ASSERT_EQUAL_UINT8(RECORD_SAVED, nvm_set_attribute(ATTR_ID_0, ATTR_ID_0_LEN, attr_0_data));
    TEST_ASSERT_EQUAL_UINT8(RECORD_SAVED, nvm_set_attribute(ATTR_ID_1, ATTR_ID_1_LEN, attr_1_data));
    TEST_ASSERT_EQUAL_UINT8(RECORD_SAVED, nvm_set_attribute(ATTR_ID_2, ATTR_ID_2_LEN, attr_2_data));
    TEST_ASSERT_EQUAL_UINT8(RECORD_SAVED, nvm_set_attribute(ATTR_ID_3, ATTR_ID_3_LEN, attr_3_data));

    TEST_ASSERT_EQUAL_UINT8(RECORD_FOUND, nvm_get_attribute(ATTR_ID_0, &attr_0_out_len, attr_0_out));
    TEST_ASSERT_EQUAL_UINT8(RECORD_FOUND, nvm_get_attribute(ATTR_ID_1, &attr_1_out_len, attr_1_out));
    TEST_ASSERT_EQUAL_UINT8(RECORD_FOUND, nvm_get_attribute(ATTR_ID_2, &attr_2_out_len, attr_2_out));
    TEST_ASSERT_EQUAL_UINT8(RECORD_FOUND, nvm_get_attribute(ATTR_ID_3, &attr_3_out_len, attr_3_out));

    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_0_data, attr_0_out, sizeof(attr_0_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_1_data, attr_1_out, sizeof(attr_1_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_2_data, attr_2_out, sizeof(attr_2_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(attr_3_data, attr_3_out, sizeof(attr_3_data));

    TEST_ASSERT_EQUAL_UINT8(RECORD_NOT_FOUND, nvm_get_attribute(ATTR_ID_W, &attr_0_out_len, attr_0_out));
    TEST_ASSERT_EQUAL_UINT8(RECORD_SAVE_FAIL, nvm_set_attribute(ATTR_ID_0, ATTR_W_LEN, flash_page_data));

    UNITY_PRINT_EOL();
    LINE_SEPARATOR();
}

/*----------------------------------------------------------------------------*/

int main(void)
{
    UNITY_BEGIN();

    RUN_TEST(test_initialization);
    LINE_SEPARATOR();

    RUN_TEST(test_flash);
    LINE_SEPARATOR();

    RUN_TEST(test_initialization);
    LINE_SEPARATOR();

    for(int i=0; i<TEST_RUN_COUNT; i++)
    {
        RUN_TEST(test_flash_driver);
        modify_data();

        LINE_SEPARATOR();
    }

    return UNITY_END();
}

