/*------------------------------------------------------------------------------
| INFORMATIONS
|
| filename: flash_driver.h
|
| author:   Krzysztof Tkaczenko
| created:  Feb 23, 2019
 `----------------------------------------------------------------------------*/

#ifndef FLASH_DRIVER_H_
#define FLASH_DRIVER_H_

/*------------------------------------------------------------------------------
| INCLUDES
`-----------------------------------------------------------------------------*/

#include <stdint.h>

/*------------------------------------------------------------------------------
| TYPEDEFS
`-----------------------------------------------------------------------------*/

typedef enum status
{
    INIT_SUCCESS,
    INIT_FAILED,
    RECORD_FOUND,
    RECORD_NOT_FOUND,
    RECORD_SAVED,
    RECORD_SAVE_FAIL,
    ERASE_FAIL,
    ERASE_SUCCESS,
    WRITE_FAIL,
    WRITE_SUCCESS,
    READ_FAIL,
    READ_SUCCESS,
    DATA_CORRUPTED,
    INVALID_PARAMS,
    INVALID_STATE
} status_t;

/*------------------------------------------------------------------------------
| FUNCTIONS PROTOTYPES
`-----------------------------------------------------------------------------*/

/**@brief Function for flash driver module initialization.
 *
 * @retval INIT_SUCCESS      If the initialization has been successful.
 * @retval INIT_FAILED       If the flash.bin could not be be created.
 */
status_t flash_driver_init(void);

/**@brief Function for reading nvm record.
 *
 * @param[in] attr_id        The record identifier.
 * @param[in] p_length       The number of read bytes.
 * @param[in] p_value        The read buffer.
 *
 * @retval RECORD_FOUND      If the record has beed read successfully.
 * @retval RECORD_NOT_FOUND  If the record was not found.
 * @retval DATA_CORRUPTED    If the page data corruption has been detected.
 */
status_t nvm_get_attribute(uint8_t attr_id, uint8_t* p_length, uint8_t* p_value);

/**@brief Function for storing nvm record.
 *
 * @param[in] attr_id        The record identifier.
 * @param[in] p_length       The number of bytes to write.
 * @param[in] p_value        The write buffer.
 *
 * @retval RECORD_SAVED      If the record has been stored successfully.
 * @retval RECORD_SAVE_FAIL  If the record store failed.
 */
status_t nvm_set_attribute(uint8_t attr_id, uint8_t length, uint8_t* p_value);

#endif /* FLASH_DRIVER_H_ */

