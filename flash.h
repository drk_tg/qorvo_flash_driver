/*------------------------------------------------------------------------------
| INFORMATIONS
|
| filename: flash.h
|
| author:   Krzysztof Tkaczenko
| created:  Feb 23, 2019
 `-----------------------------------------------------------------------------*/

#ifndef FLASH_H_
#define FLASH_H_

/*------------------------------------------------------------------------------
| INCLUDES
`-----------------------------------------------------------------------------*/

#include <stdint.h>
#include "flash_driver.h"

/*------------------------------------------------------------------------------
| DEFINES
`-----------------------------------------------------------------------------*/

#define FLASH_PAGE_SIZE       0x0100
#define FLASH_PAGES_NUM       0x04
#define FLASH_INVALID_PAGE    0xFF
#define FLASH_INVALID_ADDRESS 0xFFFFFFFF

/*------------------------------------------------------------------------------
| FUNCTIONS PROTOTYPES
`-----------------------------------------------------------------------------*/

/**@brief Function for flash module initialization.
 *
 * @retval INIT_SUCCESS      If the initialization has been successful.
 * @retval INIT_FAILED       If the flash.bin could not be be created.
 */
status_t flash_init(void);

/**@brief Function for erasing flash page.
 *
 * @param[in] page_number    The page number to be erased.
 *
 * @retval INVALID_PARAMS    If the given parameters are incorrect.
 * @retval INVALID_STATE     If the flash.bin file could not be opened.
 * @retval ERASE_FAIL        If the page could not be erased.
 * @retval ERASE_SUCCESS     If the page has been erased successfully.
 */
status_t flash_erase_page(uint8_t page_number);

/**@brief Function for writing data to flash memory.
 *
 * @param[in] buffer         The write buffer.
 * @param[in] address        The memory address to which the data should be saved.
 * @param[in] bytes_number   The number of bytes to write.
 *
 * @retval INVALID_PARAMS    If the given parameters are incorrect.
 * @retval INVALID_STATE     If flash.bin file could not be opened.
 * @retval WRITE_FAIL        If the data could not be writed.
 * @retval WRITE_SUCCESS     If the data has been writed successfully.
 */
status_t flash_write_data(uint8_t* buffer, uint32_t address, uint16_t bytes_number);

/**@brief Function for reading data from flash memory.
 *
 * @param[in] buffer         The read buffer.
 * @param[in] address        The memory address from which the data should be read.
 * @param[in] bytes_number   The number of bytes to read.
 *
 * @retval INVALID_PARAMS    If the given parameters are incorrect.
 * @retval INVALID_STATE     If flash.bin file could not be opened.
 * @retval READ_FAIL         If the data could not be read.
 * @retval READ_SUCCESS      If the data has been read successfully.
 */
status_t flash_read_data(uint8_t* buffer, uint32_t address, uint16_t bytes_number);

#endif /* FLASH_H_ */

