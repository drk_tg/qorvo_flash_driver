/*------------------------------------------------------------------------------
| INFORMATIONS
|
| filename: flash_driver.c
|
| author:   Krzysztof Tkaczenko
| created:  Feb 23, 2019
 `----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
| INCLUDES
`-----------------------------------------------------------------------------*/

#include "flash_driver.h"
#include "flash.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/*------------------------------------------------------------------------------
| DEFINES
`-----------------------------------------------------------------------------*/

#define MARKER_SIZE                  0x01
#define MARKER_PAGE_ACTIVE           0x0F
#define MARKER_PAGE_UNDER_GC         0x00
#define MARKER_RECORD_ACTIVE         0x0F
#define MARKER_RECORD_INACTIVE       0x00
#define MARKER_RECORD_NOT_PRESENT    0xFF
#define RECORD_MAX_DATA_SIZE         0xF9

#define CRC_POLYNOMIAL               0x8005
#define CRC_WIDTH                    (8 * sizeof(uint16_t))
#define CRC_TOPBIT                   (1 << (CRC_WIDTH - 1))

/*------------------------------------------------------------------------------
| TYPEDEFS
`-----------------------------------------------------------------------------*/

typedef struct nvm_record
{
    uint8_t  state;
    uint8_t  id;
    uint8_t  len;
    uint16_t crc;
    uint8_t  address;
    uint8_t  *data;
} nvm_record_t;

/*------------------------------------------------------------------------------
| LOCAL VARIABLES
`-----------------------------------------------------------------------------*/

static uint8_t active_page;
static uint32_t memory_cursor;
static uint16_t remainder;

/*------------------------------------------------------------------------------
| LOCAL FUNCTION PROTOTYPES
`-----------------------------------------------------------------------------*/

static uint8_t find_active_page(void);
static void set_active_page(uint8_t page_num);
static void reset_crc(void);
static void calculate_crc(uint8_t byte);
static uint16_t get_crc(void);
static uint32_t find_memory_cursor(uint8_t page_num);
static status_t find_record(uint8_t record_id, nvm_record_t *p_record);
static status_t save_record(nvm_record_t *p_record);
static void garbage_collector(void);

/*------------------------------------------------------------------------------
| FUNCTION DEFINITIONS
`-----------------------------------------------------------------------------*/

status_t flash_driver_init(void)
{
    active_page = find_active_page();
    if(active_page == FLASH_INVALID_PAGE)
    {
        set_active_page(0x00);
    }

    memory_cursor = find_memory_cursor(active_page);
    if(memory_cursor == FLASH_INVALID_ADDRESS)
    {
        garbage_collector();
    }

    return INIT_SUCCESS;
}

status_t nvm_get_attribute(uint8_t attrId, uint8_t* pLength, uint8_t* pValue)
{
    nvm_record_t record;

    memset(&record, 0x00, sizeof(record));
    record.data = pValue;

    uint8_t ret = find_record(attrId, &record);

    if(ret == RECORD_FOUND)
    {
        *pLength = record.len;
    }

    return ret;
}

status_t nvm_set_attribute(uint8_t attrId, uint8_t length, uint8_t* pValue)
{
    nvm_record_t record;

    record.state = MARKER_RECORD_ACTIVE;
    record.id    = attrId;
    record.len   = length;
    record.data  = pValue;

    uint8_t ret = save_record(&record);

    return ret;
}

static uint8_t find_active_page(void)
{
    for(uint8_t i=0; i<FLASH_PAGES_NUM; i++)
    {
        uint8_t page_status;
        flash_read_data(&page_status, i*FLASH_PAGE_SIZE, MARKER_SIZE);

        if(page_status == MARKER_PAGE_ACTIVE)
        {
            printf("Page %d is active.\n", i);

            return i;
        }
    }

    printf("No active page!\n");

    return FLASH_INVALID_PAGE;
}

static void set_active_page(uint8_t page_num)
{
    printf("Activating page %d.\n", page_num);

    flash_erase_page(page_num);

    uint8_t page_marker = MARKER_PAGE_ACTIVE;
    flash_write_data(&page_marker, page_num * FLASH_PAGE_SIZE, MARKER_SIZE);

    active_page = page_num;
}

static uint32_t find_memory_cursor(uint8_t page_num)
{
    uint32_t address = page_num * FLASH_PAGE_SIZE;
    address++;

    uint8_t byte;
    do
    {
        flash_read_data(&byte, address, sizeof(byte));
        address++;
        if((byte == MARKER_RECORD_ACTIVE) || (byte == MARKER_RECORD_INACTIVE))
        {
            nvm_record_t record;

            record.state = byte;
            uint8_t record_address = address - active_page*FLASH_PAGE_SIZE -1;

            reset_crc();

            flash_read_data(&record.id, address, sizeof(record.id));
            address+=sizeof(record.id);
            calculate_crc(record.id);

            flash_read_data(&record.len, address, sizeof(record.len));
            address+=sizeof(record.len);

            flash_read_data((uint8_t *)&record.crc, address, sizeof(record.crc));
            address+=sizeof(record.crc);

            flash_read_data(&record.address, address, sizeof(record.address));
            address+=sizeof(record.address);

            printf("\n");
            printf("--------------------\n");
            printf("Found record.\n");
            printf("--------------------\n");
            printf("Record state: \t%02x\n",record.state);
            printf("Record id: \t%02x\n",record.id);
            printf("Record lenght: \t%02x\n",record.len);
            printf("Record CRC: \t%04x\n",record.crc);
            printf("Record address: %02x\n",record.address);

            printf("Record data: \t");
            for(uint8_t i=0; i<record.len; i++)
            {
                flash_read_data(&byte, address, sizeof(byte));
                address++;
                calculate_crc(byte);

                printf("%02x ",byte);
            }
            printf("\n");

            if(get_crc() == record.crc)
            {
                printf("CRC check: \tPASSED\n");
            }
            else
            {
                printf("CRC check: \tFAILED!\n");
                printf("Invalidating corrupted record!\n");

                uint8_t state = MARKER_RECORD_INACTIVE;
                flash_write_data(&state, record_address + active_page*FLASH_PAGE_SIZE, sizeof(state));
            }
        }
        else if(byte == MARKER_RECORD_NOT_PRESENT)
        {
            printf("\nMemory cursor @ %08x\n",address-1);
            return address-1;
        }
        else
        {
            printf("\nPage corrupted!\n");
            return FLASH_INVALID_ADDRESS;
        }
    } while((address-page_num*FLASH_PAGE_SIZE)<FLASH_PAGE_SIZE);

    return FLASH_INVALID_ADDRESS;
}

static status_t find_record(uint8_t record_id, nvm_record_t *p_record)
{
    status_t ret = RECORD_NOT_FOUND;

    uint8_t byte;
    uint32_t address = active_page*FLASH_PAGE_SIZE;
    address++;

    do
    {
        flash_read_data(&byte, address, sizeof(byte));
        address++;
        if((byte == MARKER_RECORD_ACTIVE) || (byte == MARKER_RECORD_INACTIVE))
        {
            nvm_record_t record;

            record.state   = byte;
            record.address = address - active_page*FLASH_PAGE_SIZE - 1;

            flash_read_data(&record.id, address, sizeof(record.id));
            address+=sizeof(record.id);

            flash_read_data(&record.len, address, sizeof(record.len));
            address+=sizeof(record.len);

            flash_read_data((uint8_t *)&record.crc, address, sizeof(record.crc));
            address+=sizeof(record.crc);

            flash_read_data((uint8_t *)&record.address, address, sizeof(record.address));
            address+=sizeof(record.address);

            if((record.state == MARKER_RECORD_ACTIVE) && (record.id == record_id))
            {
                ret = RECORD_FOUND;

                if(p_record != NULL)
                {
                    p_record->state   = record.state;
                    p_record->id      = record.id;
                    p_record->len     = record.len;
                    p_record->crc     = record.crc;
                    p_record->address = record.address;
                }

                printf("\n");
                printf("--------------------\n");
                printf("Found active record.\n");
                printf("--------------------\n");
                printf("Record state: \t%02x\n",record.state);
                printf("Record id: \t%02x\n",record.id);
                printf("Record lenght: \t%02x\n",record.len);
                printf("Record CRC: \t%04x\n",record.crc);
                printf("Record address: %02x\n",record.address);

                printf("Record data: \t");
                for(uint8_t i=0; i<record.len; i++)
                {
                    flash_read_data(&byte, address, sizeof(byte));
                    address++;

                    if(p_record->data != NULL)
                    {
                        p_record->data[i] = byte;
                    }

                    printf("%02x ",byte);
                }
                printf("\n");

                break;
            }
            else
            {
                address+=record.len;
            }
        }
        else if(byte == MARKER_RECORD_NOT_PRESENT)
        {
            ret = RECORD_NOT_FOUND;
            break;
        }
        else
        {
            ret = DATA_CORRUPTED;
            break;
        }
    } while((address-active_page*FLASH_PAGE_SIZE)<FLASH_PAGE_SIZE-1);

    return ret;
}

static status_t save_record(nvm_record_t *p_record)
{
    printf("\n");
    printf("--------------------\n");
    printf("Saving record @ %02x.\n", memory_cursor);
    printf("--------------------\n");
    printf("Record state: \t%02x\n",p_record->state);
    printf("Record id: \t%02x\n",p_record->id);
    printf("Record lenght: \t%02x\n",p_record->len);

    if((p_record->state != MARKER_RECORD_ACTIVE) ||
       (p_record->state == 0) ||
       (p_record->data == NULL) ||
       (p_record->len > RECORD_MAX_DATA_SIZE))
    {
        printf("Invalid params!");
        return RECORD_SAVE_FAIL;
    }

    uint32_t address = memory_cursor
                     - active_page*FLASH_PAGE_SIZE
                     + sizeof(p_record->state)
                     + sizeof(p_record->id)
                     + sizeof(p_record->len)
                     + sizeof(p_record->crc)
                     + sizeof(p_record->address);

    if(address>FLASH_PAGE_SIZE)
    {
        printf("\nPage is full!\n");
        garbage_collector();
    }

    nvm_record_t record;
    memset(&record, 0x00, sizeof(record));
    uint8_t invalidRecordFlag = false;
    if(find_record(p_record->id, &record) == RECORD_FOUND)
    {
        invalidRecordFlag = true;
        //printf("Valid record @ %02x.\n", active_page*FLASH_PAGE_SIZE+record.address);
    }

    flash_write_data(&p_record->state, memory_cursor, sizeof(p_record->state));
    memory_cursor+=sizeof(p_record->state);

    reset_crc();

    flash_write_data(&p_record->id, memory_cursor, sizeof(p_record->id));
    memory_cursor+=sizeof(p_record->id);

    calculate_crc(p_record->id);

    flash_write_data(&p_record->len, memory_cursor, sizeof(p_record->len));
    memory_cursor+=sizeof(p_record->len);

    uint32_t crc_address = memory_cursor;
    //flash_write_data((uint8_t *)&p_record->crc, memory_cursor, sizeof(p_record->crc));
    memory_cursor+=sizeof(p_record->crc);

    uint8_t addr = memory_cursor
                  -sizeof(p_record->state)
                  -sizeof(p_record->id)
                  -sizeof(p_record->len)
                  -sizeof(p_record->crc);

    flash_write_data(&addr, memory_cursor, sizeof(p_record->address));
    memory_cursor+=sizeof(addr);

    uint8_t *byte = p_record->data;
    for(uint8_t i=0;i<p_record->len;i++)
    {
        flash_write_data(byte, memory_cursor, sizeof(uint8_t));
        memory_cursor+=sizeof(uint8_t);
        calculate_crc(*byte);
        byte++;
    }

    uint16_t crc = get_crc();
    printf("Record CRC: \t%04x\n", crc);
    flash_write_data((uint8_t *)&crc, crc_address, sizeof(remainder));

    if(invalidRecordFlag)
    {
        printf("\nDeactivate record @ %02x.\n", record.address);
        uint8_t state = MARKER_RECORD_INACTIVE;
        flash_write_data(&state, active_page*FLASH_PAGE_SIZE+record.address, sizeof(state));
    }

    return RECORD_SAVED;
}

static void garbage_collector(void)
{
    printf("Running garbage collector.\n");

    uint8_t next_page = active_page + 1;
    if(next_page >= FLASH_PAGES_NUM)
    {
        next_page=0;
    }

    uint8_t state = MARKER_PAGE_UNDER_GC;
    flash_write_data(&state, active_page*FLASH_PAGE_SIZE, sizeof(state));

    uint8_t previous_page = active_page;
    set_active_page(next_page);
    memory_cursor = find_memory_cursor(next_page);

    uint8_t byte;
    uint32_t address = previous_page*FLASH_PAGE_SIZE;
    address++;

    do
    {
        flash_read_data(&byte, address, sizeof(byte));
        address++;
        if((byte == MARKER_RECORD_ACTIVE) || (byte == MARKER_RECORD_INACTIVE))
        {
            nvm_record_t record;

            record.state   = byte;
            record.address = address - previous_page*FLASH_PAGE_SIZE - 1;

            flash_read_data(&record.id, address, sizeof(record.id));
            address+=sizeof(record.id);

            flash_read_data(&record.len, address, sizeof(record.len));
            address+=sizeof(record.len);

            flash_read_data((uint8_t *)&record.crc, address, sizeof(record.crc));
            address+=sizeof(record.crc);

            flash_read_data((uint8_t *)&record.address, address, sizeof(record.address));
            address+=sizeof(record.address);

            if(record.state == MARKER_RECORD_ACTIVE)
            {
                printf("\n");
                printf("--------------------\n");
                printf("Found active record.\n");
                printf("--------------------\n");
                printf("Record state: \t%02x\n",record.state);
                printf("Record id: \t%02x\n",record.id);
                printf("Record lenght: \t%02x\n",record.len);
                printf("Record CRC: \t%04x\n",record.crc);
                printf("Record address: %02x\n",record.address);

                flash_write_data(&record.state, memory_cursor, sizeof(record.state));
                memory_cursor+=sizeof(record.state);

                flash_write_data(&record.id, memory_cursor, sizeof(record.id));
                memory_cursor+=sizeof(record.id);

                flash_write_data(&record.len, memory_cursor, sizeof(record.len));
                memory_cursor+=sizeof(record.len);

                flash_write_data((uint8_t *)&record.crc, memory_cursor, sizeof(record.crc));
                memory_cursor+=sizeof(record.crc);

                uint8_t addr = memory_cursor
                              -sizeof(record.state)
                              -sizeof(record.id)
                              -sizeof(record.len)
                              -sizeof(record.crc);

                flash_write_data(&addr, memory_cursor, sizeof(record.address));
                memory_cursor+=sizeof(addr);

                printf("Rewriting data.\n");
                for(uint8_t i=0; i<record.len; i++)
                {
                    flash_read_data(&byte, address, sizeof(byte));
                    address++;

                    flash_write_data(&byte, memory_cursor, sizeof(byte));
                    memory_cursor+=sizeof(byte);
                }
            }
            else
            {
                address+=record.len;
            }
        }
        else if(byte == MARKER_RECORD_NOT_PRESENT)
        {
            // garbage collector finished successful
            break;
        }
        else
        {
            // found invalid marker, stop collecting garbage
            break;
        }
    } while((address-previous_page*FLASH_PAGE_SIZE)<FLASH_PAGE_SIZE-1);

    printf("\n");
    flash_erase_page(previous_page);
}

static void calculate_crc(uint8_t byte)
{
    remainder ^= (byte << (CRC_WIDTH - 8));
    for (uint8_t bit = 8; bit > 0; --bit)
    {
        if (remainder & CRC_TOPBIT)
        {
            remainder = (remainder << 1) ^ CRC_POLYNOMIAL;
        }
        else
        {
            remainder = (remainder << 1);
        }
    }
}

static void reset_crc(void)
{
    remainder = 0;
}

static uint16_t get_crc(void)
{
    return remainder;
}

