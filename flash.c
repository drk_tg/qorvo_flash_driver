/*------------------------------------------------------------------------------
| INFORMATIONS
|
| filename: flash.h
|
| author:   Krzysztof Tkaczenko
| created:  Feb 23, 2019
 `----------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------
| INCLUDES
`-----------------------------------------------------------------------------*/

#include "flash.h"
#include <stdio.h>
#include <string.h>

/*------------------------------------------------------------------------------
| LOCAL VARIABLES
`-----------------------------------------------------------------------------*/

static FILE *filePointer ;

/*------------------------------------------------------------------------------
| FUNCTION DEFINITIONS
`-----------------------------------------------------------------------------*/

status_t flash_init(void)
{
    status_t ret = INIT_FAILED;

    printf("Flash module init.\n");

    filePointer = fopen("flash.bin", "rb+");
    if(filePointer == NULL)
    {
        printf("File flash.bin don't exist!\n");
        printf("Creating flash.bin file.\n");

        filePointer = fopen("flash.bin", "wb");
        if(filePointer == NULL)
        {
            printf("Can't create new flash.bin file!\n");
            return INIT_FAILED;
        }
        else
        {
            ret = INIT_SUCCESS;

            uint8_t emptyByte = 0xFF;
            for(uint32_t i=0; i< FLASH_PAGES_NUM*FLASH_PAGE_SIZE; i++)
            {
                flash_write_data(&emptyByte, i, sizeof(emptyByte));
            }

            fclose(filePointer);
        }
    }
    else
    {
        ret = INIT_SUCCESS;
        fclose(filePointer);
    }

    return ret;
}

status_t flash_erase_page(uint8_t page_number)
{
    printf("Erasing flash page %d.\n", page_number);

    if(page_number >= FLASH_PAGES_NUM)
    {
        return INVALID_PARAMS;
    }

    filePointer = fopen("flash.bin", "rb+");
    if(filePointer != NULL)
    {
        uint32_t address = page_number * FLASH_PAGE_SIZE;
        fseek(filePointer, address, SEEK_SET);

        for(uint16_t i=0; i<FLASH_PAGE_SIZE; i++)
        {
            uint8_t emptyByte = 0xFF;
            if(fwrite(&emptyByte, sizeof(emptyByte), 1, filePointer) != sizeof(emptyByte))
            {
                return ERASE_FAIL;
            }
        }

        fclose(filePointer);
    }
    else
    {
        return INVALID_STATE;
    }

    return ERASE_SUCCESS;
}

status_t flash_write_data(uint8_t* buffer, uint32_t address, uint16_t bytes_number)
{
    //printf("Writing data %02x @ %08x.\n", *buffer, address);

    if(address >= FLASH_PAGES_NUM*FLASH_PAGE_SIZE)
    {
        return INVALID_PARAMS;
    }

    filePointer = fopen("flash.bin", "rb+");
    if(filePointer != NULL)
    {
        fseek(filePointer, address, SEEK_SET);
        for(uint16_t i=0; i<bytes_number; i++)
        {
            uint8_t byte = fgetc(filePointer);
            byte &= *buffer;

            fseek(filePointer, address+i, SEEK_SET);
            if(fwrite(&byte, sizeof(byte), 1, filePointer) != sizeof(byte))
            {
                return WRITE_FAIL;
            }
            buffer++;
        }

        fclose(filePointer);
    }
    else
    {
        return INVALID_STATE;
    }

    return WRITE_SUCCESS;
}

status_t flash_read_data(uint8_t* buffer, uint32_t address, uint16_t bytes_number)
{
    //printf("Reading data.\n");

    if(address >= FLASH_PAGES_NUM*FLASH_PAGE_SIZE)
    {
        return INVALID_PARAMS;
    }

    filePointer = fopen("flash.bin", "rb+");
    if(filePointer != NULL)
    {
        fseek(filePointer, address, SEEK_SET);
        for(uint16_t i=0; i<bytes_number; i++)
        {
            *buffer = fgetc(filePointer);
            buffer++;
        }

        fclose(filePointer);
    }
    else
    {
        return INVALID_STATE;
    }

    return READ_SUCCESS;
}

